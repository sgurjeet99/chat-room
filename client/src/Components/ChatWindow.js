import React, { useState, useEffect } from 'react'
import {
  CircularProgress
} from '@material-ui/core'
import { GET_EVENTS } from '../config'
import InputComponent from './InputComponent'

export default function ChatWindow(props) {
  
  useEffect(() => {
  }, [ props.messages.length ])
  
  console.log('chatwindow', props.messages)
  return (
    <div id='chatWindow'>
      {props.messages.map(m => {
        if(!m.text) return
        return (
          <div id='message'>
            <span>{m.avatar}</span>
            {m.text}
          </div>
        )
      })}
      {props.isHost ? <InputComponent /> : <div id='hostOnlyRibbon'>Only host can post messages</div>}
    </div>
  )
}