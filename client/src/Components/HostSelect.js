import React, { useState, useEffect } from 'react'
import { 
  Button
} from '@material-ui/core'
import { sendMessage } from '../ServiceClass'

export default function HostSelect(props) {

  const handleSetHost = (i) => {
    props.setAvatar(i)
    sendMessage({
      avatar: i,
      id: props.id
    }, 1)
  }
  
  return (
    <div id='hostSelection'>
      {[0,1,2,3,4].map(i => (
        <Button variant='outlined' key={i} onClick={() => handleSetHost(i)}>
          {i}
        </Button>
      ))}
    </div>
  )
}