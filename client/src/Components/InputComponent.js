import React, { useState, useEffect, useRef } from 'react'
import {
  Fab, TextField
} from '@material-ui/core'
import { Send } from '@material-ui/icons'
import EmojiPicker from 'emoji-picker-react';
import { sendMessage } from '../ServiceClass'

export default function InputComponent(props) {

  const [ text, setText ] = useState('')
  const [ isLoading, setIsLoading] = useState(false)
  const inputRef = useRef()

  const handleTextChange = (e) => {
    setText(e.target.value)
  }

  const selectEmoji = (e, emojiObject) => {
    console.log(emojiObject, inputRef.current)
    // const start = inputRef.current.selectionStart
    // const end = inputRef.current.selectionEnd
    // const text = inputRef.current.value
    // const before = text.substring(0, start)
    // const after  = text.substring(end, text.length)
    // inputRef.current.value = (before + emojiObject.unified + after)
    // inputRef.current.selectionStart = inputRef.current.selectionEnd = start + emojiObject.emoji.length
    // inputRef.current.focus()
  }

  const handleMessage = () => {
    setIsLoading(true)
    sendMessage({
      text,
      avatar: props.avatar
    })
    .then(data => {
      setIsLoading(false)
      setText('')
    })
    .catch(err => setIsLoading(false))
  }

  return (
    <div id='textField'>
      <EmojiPicker onEmojiClick={selectEmoji}/>
      <TextField
        ref={inputRef}
        fullWidth
        label='Type and hit send...'
        value={isLoading ? "Sending message..." : text}
        onChange={handleTextChange}
      />
      <Fab aria-label='send' onClick={handleMessage}>
        <Send/>
      </Fab>
    </div>
  )
}