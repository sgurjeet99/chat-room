import { BASE_URL, GET_EVENTS, SEND_MESSAGE } from './config'
import io from "socket.io-client";
const socket = io(BASE_URL)
//0: message, 1: set host message
export const sendMessage = (data, type = 0) => {
  let event = type ? 'setHost' : 'message'
  socket.emit(event, data)
  // console.log(JSON.stringify(body))
  // return fetch(SEND_MESSAGE, {
  //   method: 'POST',
  //   body: JSON.stringify(body)
  // })
  // .then(res => res.json())
}