export const BASE_URL = 'http://localhost:8001'

export const SEND_MESSAGE = BASE_URL + '/message'
export const GET_EVENTS = BASE_URL + '/events'