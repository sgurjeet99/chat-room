import './App.css';
import React, { useState, useEffect } from 'react'
import {
  CircularProgress
} from '@material-ui/core'
import { GET_EVENTS, BASE_URL } from './config'
import HostSelect from './Components/HostSelect'
import ChatWindow from './Components/ChatWindow'
import io from "socket.io-client";

function App() {

  const [ isUserHost, setIsUserHost ] = useState()
  const [ isHostSet, setIsHostSet ] = useState(false)
  const [ clientId, setClientId ] = useState('')
  const [ messages, setMessages ] = useState([])
  const [ isLoading, setIsLoading ] = useState(true)
  const [ socket, setSocket ] = useState(null);
  const [ avatar, setClientAvatar ] = useState(null);

  useEffect(() => {
    setSocket(io(BASE_URL))
    if(!socket)  return
    socket.on('connect', socket => {
      setClientId(socket.id)
      setIsLoading(false)
    })
    socket.on('setHost', data => {
      if(!data.hostSet) {
        setIsHostSet(false)
      }
      else if(data.hostSet && data.host == socket.id) {
        setIsHostSet(true)
        setIsUserHost(true)
      }
    })
    socket.on('message', data => {
      setMessages(m => m.concat(data))
    })
  }, [ messages.length, isUserHost, isHostSet, socket ])

  const setAvatar = (i) => {
    setClientAvatar(i)
  }

  return (
    <div className="App">
      {isLoading ? (
        <CircularProgress/>
      ) : (
        !isHostSet ? <HostSelect id={clientId} socket={socket} setAvatar={setAvatar}/> : <ChatWindow isHost={isUserHost} id={clientId} messages={messages} avatar={avatar} />
      )}
    </div>
  );
}

export default App;
