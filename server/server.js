const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = require('socket.io')(server, { 'origins': 'http://localhost:3000'})

const cors = require('cors')
const bodyParser = require('body-parser');
const PORT = 8001;
let host = ''
// let clients = {}

// app.use(cors({
//   credentials: true,
//   origin: 'http://localhost:3000'
// }));
app.use(bodyParser.json({ type: '*/*' }))
app.use(bodyParser.urlencoded({extended: false}));

io.on('connect', client => {
  console.log(client)
  if(!host) {
    client.emit('setHost', {
      hostSet: false
    })
  }
  client.on('setHost', data => {
    console.log(data)
    host = data.id
    io.emit('setHost', {
      hostSet: true,
      host
    })
  })
  client.on('message', data => {
    io.emit('message', data)
  })
})

io.on('disconnect', client => {
  if(client.id == host) {
    host = ''
    io.emit('setHost', {
      hostSet: false
    })
  }
})
// app.post('/message', addMessage);
// app.get('/events', eventsHandler);
// // app.get('/status', (req, res) => res.json({clients: clients.length}));

// function eventsHandler(req, res) {
//   const headers = {
//     'Content-Type': 'text/event-stream',
//     'Access-Control-Allow-Origin': '*',
//     'Connection': 'keep-alive',
//     'Cache-Control': 'no-cache'
//   };
//   res.writeHead(200, headers);

//   // using time as unique id for clients
//   const clientId = Date.now();

//   let hostData = {
//     isHost: host === clientId,
//     hostSet: !!host,
//     clientId
//   }

//   let responseData = {
//     data: messages,
//     host: hostData
//   }
//   const data = `data: ${JSON.stringify(responseData)}\n\n`;
//   res.write(`event: message\n`);
//   res.write(data);
  
//   const newClient = {
//     id: clientId,
//     res,
//     avatar: data.avatar,
//     host: host === clientId
//   }

//   clients.push(newClient);

//   // handle exit of a client
//   req.on('close', () => {
//     console.log(`${clientId} Connection closed`);
//     clients = clients.filter(c => c.id !== clientId);
    
//     // if host closed it's connection, allow others to become host
//     if(host === clientId){
//       host = ''
//       sendEventsToAll({ event: 'setHost' })
//     }
//   });
// }

// //send events to all clients
// function sendEventsToAll(data) {
//   clients.forEach(c => {
//     let hostData = {
//       isHost: host == c.id,
//       hostSet: !!host,
//       clientId: c.id
//     }
//     if(host == c.id) {
//       c.host = true
//       c.avatar = data.avatar
//     }
    
//     let responseData = {
//       data,
//       host: hostData
//     }
//     c.res.write(`event: message\n`);
//     c.res.write(`data: ${JSON.stringify(responseData)}\n\n`)
//   })
// }

// function addMessage(req, res) {
//   const newMessage = req.body
//   if(!host && newMessage.event === 'setHost') {
//     //event, data: id(string), avatar(index)
//     host = newMessage.data.id
//   }
//   else if(newMessage.event === 'message') {
//     //event: message, text, avatar index
//     messages.push(newMessage.data)
//     res.json(newMessage.data)
//   }
//   return sendEventsToAll(newMessage.data);
// }

server.listen(PORT, () => console.log("Server is listening at 8001..."))